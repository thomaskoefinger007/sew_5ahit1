﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankenanwendungMV
{
    class Sparkonto : Konto
    {
        double bonus = 0.3;

        public override bool Abhebung(double betrag)
        {
            if (Betrag - betrag > 0)
                base.Abhebung(betrag);
            Console.WriteLine("Abheben nicht möglich");
            return false;
        }
        public void Bonus()
        {
            Console.WriteLine(bonus);
        }
        
    }
}
