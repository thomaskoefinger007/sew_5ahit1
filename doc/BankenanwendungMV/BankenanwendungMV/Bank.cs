﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankenanwendungMV
{
    class Bank
    {
        public List<Konto> Kunden = new List<Konto>();
        public string Beschreibung { get; set; }
        
        //Indexer
        public Konto this[int index]
        {
            get;
            set;
        }

        public void Abschluss()
        {
            foreach (var item in Kunden)
            {
                item.Betrag += item.Betrag * Konto.Zinssatz;

            }
        }
        public void Add(Konto neu)
        {
            Kunden.Add(neu);
        }
        public void Add(List<Konto> neu)
        {
            neu = new List<Konto>();
        }
        public void Sort() {
            Kunden.Sort();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Beschreibung + "\n");
            foreach (Konto k in Kunden)
	        {
                sb.Append(k);
                
	        }
            sb.Append("-------------");
            return sb.ToString();
        }
        public void Ueberweisung(double betrag, Konto k1, Konto k2)
        {

        }

    }
}
