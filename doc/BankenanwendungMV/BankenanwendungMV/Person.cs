﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankenanwendungMV
{
    class Person :IComparable
    {
        private int dummiprivate;
        protected double dummyprotected;
        public string dummystring;
        public string Familienname { get; set; }
        public string Vorname { get; set; }

       
        public override string ToString()
        {

            string name = Familienname + " " + Vorname;
            return name;
        }


        public int CompareTo(object obj)
        {
            return Familienname.CompareTo(((Person)obj).Familienname);
        }
    }
}
