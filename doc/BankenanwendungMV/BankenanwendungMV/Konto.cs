﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankenanwendungMV
{
    class Konto :IComparable
    {
        public Person Inhaber { get; set; }
        public double Betrag { get; set; }
        public string Nr { get; set; }
        internal static double Zinssatz { get; set; }

        static Konto()
        {
            Zinssatz = 0.2;
        }

        virtual public bool Abhebung(double betrag)
        {
            Betrag -= betrag;
            return true;
        }
        public int CompareTo(object obj)
        {
            return Nr.CompareTo(((Konto)obj).Nr);
        }
        public void Einzahlung(double betrag)
        {
            Betrag += betrag;
        }
        static void Überweisung(double betrag, Konto k1, Konto k2)
        {
            k2.Abhebung(betrag);
            if (k2.Abhebung(betrag))
            k1.Einzahlung(betrag);
        }
        public void Zinsen(double neu)
        {
            Zinssatz = neu;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("---------------\n");
            sb.Append(Inhaber + "\n");
            sb.Append(Betrag + "\n");
            sb.Append(Nr + "\n");
            sb.Append(Zinssatz + "\n");
       
            return sb.ToString();
        }
        
        
    }
}
