﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormsAnimation
{
    class Spiel
    {
        int[,] feld;
        int spieler = 2;

        public Spiel(int anz)
        {
            feld = new int[anz + 2, anz + 2];
        }

        public int Clicked(int i, int j)
        {
            if (feld[i, j] == 0)
            {
                feld[i, j] = spieler;
                spieler = spieler % 2 + 1;
                return spieler;//!!!
            }
            return 0;
        }
       
    }
}
