﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Spiel
    {
        int[,] feld;
        int spieler = 2;

        public Spiel(int anz)
        {
            feld = new int[anz + 2, anz + 2];
        }

        public int Clicked(int i, int j)
        {
            if (feld[i, j] == 0)
            {
                feld[i, j] = spieler;
                spieler = spieler % 2 + 1;
                return spieler;//!!!
            }
            return 0;
        }
        public bool Win()
        {
            for (int i = 0; i < feld.GetLength(0); i++)
            {
                for (int j = 0; j < feld.GetLength(1); j++)
                {
                    if (feld[i, j] != 0)
                    {
                        if (feld[i, j] == feld[i, j + 1] && feld[i, j + 1] == feld[i, j + 2])
                            return true;
                        else if (feld[i, j] == feld[i + 1, j] && feld[i + 1, j] == feld[i + 2, j])
                            return true;
                        else if (feld[i, j] == feld[i + 1, j + 1] && feld[i + 1, j + 1] == feld[i + 2, j + 2])
                            return true;
                    }
                    // if (feld[i+2, j+2] == feld[i + 1, j + 1] && feld[i + 1, j + 1] == feld[i + 2, j])
                    //     return true;
                }
            } return false;
        }
    }
}
