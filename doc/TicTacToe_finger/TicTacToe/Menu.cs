﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Menu : Form
    {
        public int wert { get { return (int)numericUpDown1.Value; } }
        public string imgp1 { get { return openFileDialog1.FileName; } }
        public string imgp2 { get { return openFileDialog2.FileName; } }
        public Menu()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "images|*jpg";
            openFileDialog1.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            openFileDialog2.Filter = "images|*jpg";
            openFileDialog2.ShowDialog();
        }

        
    }
}
