﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace GDI_Kennzeichen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            
            int x = 100;
            int y = 100;

            e.Graphics.FillRectangle(Brushes.White, new Rectangle(x, y, 200, 50));

            e.Graphics.DrawLine(new Pen(Color.Red), x, y + 3, x+200, y+3);
            e.Graphics.DrawLine(new Pen(Color.Red), x, y + 5, x + 200, y + 5);

            e.Graphics.DrawLine(new Pen(Color.Red), x, y + 47, x + 200, y + 47);
            e.Graphics.DrawLine(new Pen(Color.Red), x, y + 45, x + 200, y + 45);

        }
       

        
    }
    

}
