﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GDI_Linien
{   
    public partial class Form1 : Form
    {
        List<Point> p = new List<Point>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            p.Clear();
            pictureBox1.Invalidate();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            Graphics g = e.Graphics;
            if(p.Count >=2)
                g.DrawLines(new Pen(Brushes.Black, 3), p.ToArray());
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            
            p.Add(new Point(e.X, e.Y));
            pictureBox1.Invalidate();

        }
        

       
    }
  
}
